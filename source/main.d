#!/usr/bin/env -S rdmd --compiler=ldc2

import amalthea.fs, amalthea.langlocal, amalthea.net, amalthea.sys;
alias jv = amalthea;
import std.algorithm, std.base64, std.string, std.regex;

immutable app = "pageguard";
immutable appVersion = import("version").strip;


int main(string[] args) {
    if (args.length == 2 && args[1] == "--help") {
        writeln(appVersion);
        return 0;
    }
    if (args.length == 2 && args[1] == "--help" || args.length != 2) {
        writeln(jv.sys.readAppHelpText(app));
        return 0;
    }
    string url = args[1];
    try {
        savePage(url);
    } catch(Exception e) {
        stderr.writeln(e.msg);
        return 1;
    }
    return 0;
}


string getContentTypeByURL(in char[] url) {
    import std.net.curl : HTTP, options;
    auto http = HTTP(url);
    http.verifyPeer = false;
    http.perform;
    auto header = http.responseHeaders;
    string contentType;
    foreach(key, value; header) {
        if (key.toLower == "content-type") {
            contentType = value;
            break;
        }
    }
    return contentType;
}


bool isHTML(string url) {
    string contentType;
    try {
        contentType = getContentTypeByURL(url);
        if (contentType.canFind("text/html")) {
            return true;
        }
    } catch(Exception e) {
        stderr.writeln(e.msg);
        return false;
    }
    return false;
}


/*******************************************************************************
 * Function for saving HTML-pages
 * with resources and conversion of links,
 * returns title of HTML-page.
 * Cut as deprecated function from the Amalthea library.
 */
string savePage(string url, string targetDir = ".", bool monolithic = true) {
    if (!isHTML(url)) {
        throw new Exception("This URL does not lead to HTML-page.");
    }
    auto title = url.getHTMLPageTitle.replace("/", "|");
    auto pageDir = targetDir.stripRight('/') ~ "/" ~ title;
    auto contentDir = pageDir ~ "/content/";
    auto baseAddress = getBaseAddressFromURL(url);
    if (!pageDir.exists) mkdirRecurse(pageDir);
    if (!monolithic && !contentDir.exists) mkdir(contentDir);
    string page = "<!-- " ~ url ~ " -->\n";
    page ~= jv.net.getPage(url).toString;
    
    replaceEncodingLabelToUnicode(page);
    auto srcValues = page.matchAll(ctRegex!(`[sS][rR][cC]="([^"]*)`));
    auto hrefValues = page.matchAll(ctRegex!(`[hH][rR][eE][fF]="([^"]*)`));
    string[] resourceAddresses;
    foreach(el; srcValues)  resourceAddresses ~= el[1];
    foreach(el; hrefValues) resourceAddresses ~= el[1];

    auto cwd = getcwd();
    cd(pageDir);
    foreach(addr; resourceAddresses) {
        if (handleIfPicture(page, url, addr, monolithic))    continue;
        if (handleIfSourceCode(page, url, addr, monolithic)) continue;
        //if (addr.startsWith("//")) page = page.replace(addr, addr[2 .. $]);
    }
    cd(cwd);
    std.file.write(pageDir ~ "/index.html", page);
    return title;
}


private auto getBaseAddressFromURL(string url) {
    auto lastSlashIndex = url.lastIndexOf('/');
    string filename = url[lastSlashIndex+1 .. $];
    string baseAddress = url[0 .. lastSlashIndex+1];
    return baseAddress;
}


private ref string replaceEncodingLabelToUnicode(return ref string html) {
    foreach(label; encodingLabels) {
        html = html.replace(`charset="`~label~`"`, `charset=UTF-8`)
                   .replace(`charset='`~label~`'`, `charset=UTF-8`)
                   .replace(`charset=`~label~``,   `charset=UTF-8`);
    }
    return html;
}


private immutable picExtensions = [
    ".bmp", ".gif", ".jp2", ".jpg", ".jpeg", ".png", ".tif", ".tiff"
];


private immutable srcExtensions = [".css", ".js"];


private bool handleIfPicture(ref string page,
                             string pageURL,
                             string resourceAddr,
                             bool monolithic) {
    alias url = pageURL;
    alias addr = resourceAddr;
    bool urlIsPicture;
    auto lowerAddr = addr.toLower;
    foreach(ext; picExtensions) {
        if (lowerAddr.endsWith(ext) || lowerAddr.indexOf(ext~"?") != -1) {
            urlIsPicture = true;
            break;
        }
    }
    if (!urlIsPicture) return false;
    auto fsaddress = "content/" ~ getFileNameFromURL(addr);
    string picURL;
    if (addr.startsWith("//")) {
        picURL = addr[2 .. $];
    } else if (addr.startsWith("http")) {
        picURL = addr;
    } else {
        picURL = getSiteAddressFromURL(url) ~ addr;
    }
    ubyte[] pic;
    try {
        pic = jv.net.getRaw(picURL);
    } catch (StdNetException e) {
        debug stderr.writeln("Warning: ", e.msg, ": ", picURL);
        return true;
    }
    if (!monolithic) {
        std.file.write(fsaddress, pic);
        page = page.replace(addr, "./"~fsaddress);
    } else {
        string encoded = Base64.encode(pic);
        string extension = addr.split('.')[$-1].split('?')[0];
        string source = "data:image/" ~ extension ~ ";base64," ~ encoded;
        page = page.replace(addr, source);
    }
    return true;
}


private bool handleIfSourceCode(ref string page,
                                string pageURL,
                                string resourceAddr,
                                bool monolithic) {
    alias url = pageURL;
    alias addr = resourceAddr;
    bool urlIsJS;
    bool urlIsCSS;
    auto lowerAddr = addr.toLower;
    if (lowerAddr.endsWith(".js"))             urlIsJS = true;
    else if (-1 != lowerAddr.indexOf(".js?"))  urlIsJS = true;
    else if (lowerAddr.endsWith(".css"))       urlIsCSS = true;
    else if (-1 != lowerAddr.indexOf(".css?")) urlIsCSS = true;

    bool urlIsSourceCode = urlIsJS || urlIsCSS;
    if (!urlIsSourceCode) return false;
    auto fsaddress = "content/" ~ getFileNameFromURL(addr);
    string resURL;
    if (addr.startsWith("//")) {
        resURL = addr[2 .. $];
    } else if (addr.startsWith("http")) {
        resURL = addr;
    } else {
        resURL = getSiteAddressFromURL(url) ~ addr;
    }
    string resource;
    try {
        resource = jv.net.getPage(resURL).toString;
    } catch (StdNetException e) {
        debug stderr.writeln("Warning: ", e.msg, ": ", resURL);
        return true;
    }
    
    if (!monolithic) {
        std.file.write(fsaddress, resource);
        page = page.replace(addr, "./"~fsaddress);
    } else {
        ssize_t index = indexOf(page, addr);
        if (urlIsJS) {
            resource = handleClosedTagCases(resource);
            index = index + page[index .. $].indexOf("</script>");
            index = index + page[index .. $].indexOf(">") + 1;
            resource =
                "\n<!-- begin "~addr~"--><script type='text/javascript'>\n"
                ~ resource ~
                "\n</script><!-- end -->\n";
        }
        else
            index = index + page[index .. $].indexOf(">") + 1;
        if (urlIsCSS) {
            resource =
                "\n" ~
                `<style type="text/css">`
                ~ "\n" ~ resource ~ "\n</style>\n";
        }

        page = page[0 .. index] ~ resource ~ page[index .. $];
        page = page.replace(`"`~addr~`"`, `""`);
        page = page.replace(`'`~addr~`'`, `""`);
        page = page.replace(`=`~addr, `=""`);
    }
    return true;
}


private auto getSiteAddressFromURL(string url) {
    auto temp = url.findSplitAfter("//");
    string protocol = temp[0];
    string address = temp[1];
    auto firstSlashIndex = address.indexOf('/');
    string site;
    if (firstSlashIndex == -1) {
        site = address;
    } else {
        site = address[0 .. firstSlashIndex+1];
    }
    return site;
}

private auto getFileNameFromURL(string url) {
    auto temp = url.findSplitAfter("//");
    string protocol = temp[0];
    string address = temp[1];
    auto lastSlashIndex = address.lastIndexOf('/');
    string filename = address[lastSlashIndex+1 .. $];
    if (filename.empty) {
        filename = "index.html";
        url ~= filename;
    }
    filename = filename.split("?")[0];
    filename = filename.split("#")[0];
    return filename;
}


private string handleClosedTagCases(string resource) {
    string result = resource.idup;
    char q = '"';
    auto index = result.indexOf("</");
    while(index != -1) {
        foreach_reverse(i, ch; result[0 .. index]) {
            char prev_ch = result[i-1];
            if (ch == '"') {
                q = '"';
                break;
            } else if (ch == '\'') {
                q = '\'';
                break;
            }
        }
        q = '\''; //temporaly
        result = result.replace(`</`, `<`~q~`+`~q~`/`);
        index = result.indexOf("</");
    }
    return result;
}



//source: https://encoding.spec.whatwg.org/
immutable string[] encodingLabels = [
/*UTF-8*/          "unicode-1-1-utf-8", "utf-8", "utf8", 
/*IBM866*/         "866", "cp866", "csibm866", "ibm866",
/*ISO-8859-2*/     "csisolatin2", "iso-8859-2", "iso-ir-101", "iso8859-2",
                   "iso88592", "iso_8859-2", "iso_8859-2:1987", "l2", "latin2",
/*ISO-8859-3*/     "csisolatin3", "iso-8859-3", "iso-ir-109", "iso8859-3",
                   "iso88593", "iso_8859-3", "iso_8859-3:1988", "l3", "latin3",
/*ISO-8859-4*/     "csisolatin4", "iso-8859-4", "iso-ir-110", "iso8859-4",
                   "iso88594", "iso_8859-4", "iso_8859-4:1988", "l4", "latin4",
/*ISO-8859-5*/     "csisolatincyrillic", "cyrillic", "iso-8859-5", "iso-ir-144",
                   "iso8859-5", "iso88595", "iso_8859-5", "iso_8859-5:1988", 
/*ISO-8859-6*/     "arabic", "asmo-708", "csiso88596e", "csiso88596i",
                   "csisolatinarabic", "ecma-114", "iso-8859-6", "iso-8859-6-e",
                   "iso-8859-6-i", "iso-ir-127", "iso8859-6", "iso88596",
                   "iso_8859-6", "iso_8859-6:1987", 
/*ISO-8859-7*/     "csisolatingreek", "ecma-118", "elot_928", "greek", "greek8",
                   "iso-8859-7", "iso-ir-126", "iso8859-7", "iso88597",
                   "iso_8859-7", "iso_8859-7:1987", "sun_eu_greek", 
/*ISO-8859-8*/     "csiso88598e", "csisolatinhebrew", "hebrew", "iso-8859-8",
                   "iso-8859-8-e", "iso-ir-138", "iso8859-8", "iso88598",
                   "iso_8859-8", "iso_8859-8:1988", "visual", 
/*ISO-8859-8-I*/   "csiso88598i", "iso-8859-8-i", "logical",
/*ISO-8859-10*/    "csisolatin6", "iso-8859-10", "iso-ir-157", "iso8859-10",
                   "iso885910", "l6", "latin6", 
/*ISO-8859-13*/    "iso-8859-13", "iso8859-13", "iso885913", 
/*ISO-8859-14*/    "iso-8859-14", "iso8859-14", "iso885914", 
/*ISO-8859-15*/    "csisolatin9", "iso-8859-15", "iso8859-15", "iso885915",
                   "iso_8859-15", "l9", 
/*ISO-8859-16*/    "iso-8859-16", 
/*KOI8-R*/         "cskoi8r", "koi", "koi8", "koi8-r", "koi8_r", 
/*KOI8-U*/         "koi8-ru", "koi8-u", 
/*macintosh*/      "csmacintosh", "mac", "macintosh", "x-mac-roman", 
/*windows-874*/    "dos-874", "iso-8859-11", "iso8859-11", "iso885911",
                   "tis-620", "windows-874", 
/*windows-1250*/   "cp1250", "windows-1250", "x-cp1250", 
/*windows-1251*/   "cp1251", "windows-1251", "x-cp1251", 
/*windows-1252*/   "ansi_x3.4-1968", "ascii", "cp1252", "cp819", "csisolatin1",
                   "ibm819", "iso-8859-1", "iso-ir-100", "iso8859-1",
                   "iso88591", "iso_8859-1", "iso_8859-1:1987", "l1", "latin1",
                   "us-ascii", "windows-1252", "x-cp1252", 
/*windows-1253*/   "cp1253", "windows-1253", "x-cp1253", 
/*windows-1254*/   "cp1254", "csisolatin5", "iso-8859-9", "iso-ir-148",
                   "iso8859-9", "iso88599", "iso_8859-9", "iso_8859-9:1989",
                   "l5", "latin5", "windows-1254", "x-cp1254", 
/*windows-1255*/   "cp1255", "windows-1255", "x-cp1255", 
/*windows-1256*/   "cp1256", "windows-1256", "x-cp1256", 
/*windows-1257*/   "cp1257", "windows-1257", "x-cp1257", 
/*windows-1258*/   "cp1258", "windows-1258", "x-cp1258", 
/*x-mac-cyrillic*/ "x-mac-cyrillic", "x-mac-ukrainian", 
/*GBK*/            "chinese", "csgb2312", "csiso58gb231280", "gb2312", "gb_2312",
                   "gb_2312-80", "gbk", "iso-ir-58", "x-gbk", 
/*gb18030*/        "gb18030", 
/*Big5*/           "big5", "big5-hkscs", "cn-big5", "csbig5", "x-x-big5", 
/*EUC-JP*/         "cseucpkdfmtjapanese", "euc-jp", "x-euc-jp", 
/*ISO-2022-JP*/    "csiso2022jp", "iso-2022-jp", 
/*Shift_JIS*/      "csshiftjis", "ms932", "ms_kanji", "shift-jis", "shift_jis",
                   "sjis", "windows-31j", "x-sjis", 
/*EUC-KR*/         "cseuckr", "csksc56011987", "euc-kr", "iso-ir-149", "korean",
                   "ks_c_5601-1987", "ks_c_5601-1989", "ksc5601", "ksc_5601",
                   "windows-949", 
/*replacement*/    "csiso2022kr", "hz-gb-2312", "iso-2022-cn",
                   "iso-2022-cn-ext", "iso-2022-kr", "replacement", 
/*UTF-16BE*/       "utf-16be", 
/*UTF-16LE*/       "utf-16", "utf-16le", 
/*x-user-defined*/ "x-user-defined"
];

