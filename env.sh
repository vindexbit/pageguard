#!/bin/bash
DC=$1
# get_phobos_options
REQUEST=$2
ARG=$3

LDC2_STATIC_OPTS="-defaultlib=:libphobos2-ldc.a,:libdruntime-ldc.a,:libz.a"
LDC2_STATIC_OPTS+=" -link-defaultlib-shared=false"
GDC_STATIC_OPTS="-static-libphobos"
DMD_STATIC_OPTS=""

# ldc2 flags
LDC2_DEBUG_OPTIONS="-d-debug --gc"
LDC2_OPTIM_OPTIONS="-O --release --gc"
# dmd flags
DMD_DEBUG_OPTIONS="-debug -g"
DMD_OPTIM_OPTIONS="-O -release"
# gdc flags
GDC_DEBUG_OPTIONS="-fdebug"
GDC_OPTIM_OPTIONS="-O2 -frelease"


if [[ $REQUEST == "get_phobos_options" && $ARG == static ]]; then
    echo $(eval echo \$${DC^^}_STATIC_OPTS)
elif [[ $REQUEST == "get_amalthea_options" ]]; then
    [[ ${ARG} == dynamic ]] && LIBEXT=so.0 || LIBEXT=a
    echo "-L-l:libamalthea-${DC}.${LIBEXT}"
elif [[ $REQUEST == "get_release_options" ]]; then
    echo $(eval echo \$${DC^^}_OPTIM_OPTIONS)
elif [[ $REQUEST == "get_debug_options" ]]; then
    echo $(eval echo \$${DC^^}_DEBUG_OPTIONS)
fi

if [[ $REQUEST == "get_doc_dir" ]]; then
    read -ra distr_id_arr <<< "`lsb_release -i`"
    DISTRIBUTION=${distr_id_arr[2]}
    if [[ "${DISTRIBUTION}" != "openSUSE" ]]; then
        echo share/doc
    else
        echo share/doc/packages
    fi
fi
